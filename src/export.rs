use crate::error::BlockResult;
use crate::monitor::Monitor;
use serde::{Deserialize, Serialize};
use std::thread::{self, JoinHandle};

pub mod bench;
pub mod nbd;

pub struct Export {
    pub id: String,
    #[allow(dead_code)]
    task: JoinHandle<()>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct ExportConfig {
    id: String,
    node_name: String,

    #[serde(flatten)]
    driver: ExportDriverConfig,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type", rename_all = "kebab-case")]
pub enum ExportDriverConfig {
    Nbd(nbd::Config),
    Bench(bench::Config),
}

impl Export {
    pub async fn new(opts: ExportConfig, mon: &Monitor) -> BlockResult<Export> {
        let node = mon.lookup_node(&opts.node_name)?;

        let task = match opts.driver {
            ExportDriverConfig::Nbd(o) => thread::spawn(move || nbd::worker(node, o)),
            ExportDriverConfig::Bench(o) => thread::spawn(move || bench::worker(node, o)),
        };

        Ok(Export { id: opts.id, task })
    }
}
