use crate::error::{BlockError, BlockResult};
use crate::export::{Export, ExportConfig};
use crate::node::{Node, NodeConfig};
use std::collections::{HashMap, LinkedList};
use std::sync::{Arc, Condvar, Mutex, Weak};
use structopt::StructOpt;

#[derive(Copy, Clone, PartialEq, Eq)]
pub enum MonitorStatus {
    Running,
    Quit,
}

#[derive(Default)]
pub struct Monitor {
    nodes: LinkedList<Arc<Node>>,
    node_map: HashMap<String, Weak<Node>>,

    exports: HashMap<String, Export>,

    status: Mutex<MonitorStatus>,
    status_change: Condvar,
}

#[derive(StructOpt)]
struct Opt {
    #[structopt(long)]
    blockdev: Vec<String>,

    #[structopt(long)]
    export: Vec<String>,
}

impl Default for MonitorStatus {
    fn default() -> Self {
        MonitorStatus::Running
    }
}

impl Monitor {
    pub fn new() -> Self {
        Monitor::default()
    }

    pub async fn parse_args(&mut self) -> BlockResult<()> {
        let opt = Opt::from_args();

        for blockdev in opt.blockdev {
            self.blockdev_add(&blockdev)
                .await
                .map_err(|e| e.prepend(&blockdev))?;
        }

        for export in opt.export {
            self.export_add(&export)
                .await
                .map_err(|e| e.prepend(&export))?;
        }

        Ok(())
    }

    pub fn wait(self) -> BlockResult<()> {
        let mut status = self.status.lock().unwrap();
        while *status != MonitorStatus::Quit {
            status = self.status_change.wait(status).unwrap();
        }
        Ok(())
    }

    pub async fn blockdev_add(&mut self, json: &str) -> BlockResult<()> {
        let node_opts: NodeConfig = serde_json::from_str(json)?;
        let node = Node::new(node_opts, &mut self.node_map).await?;
        self.nodes.push_back(node);

        Ok(())
    }

    pub async fn export_add(&mut self, json: &str) -> BlockResult<()> {
        let export_opts: ExportConfig = serde_json::from_str(json)?;
        let export = Export::new(export_opts, self).await?;

        if self.exports.contains_key(&export.id) {
            return Err(BlockError::from_desc(format!(
                "Export ID \"{}\" is already in use",
                export.id
            )));
        }
        let existing = self.exports.insert(export.id.clone(), export);
        assert!(existing.is_none());

        Ok(())
    }

    pub fn lookup_node(&self, name: &str) -> BlockResult<Arc<Node>> {
        let node = self.node_map.get(name).ok_or_else(|| {
            BlockError::from_desc(format!("Node with name \"{}\" not found", name))
        })?;

        node.upgrade().ok_or_else(|| {
            BlockError::from_desc(format!("Node with name \"{}\" has been deleted", name))
        })
    }
}
