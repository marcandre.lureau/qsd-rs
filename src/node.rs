use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Weak};

pub mod blkio;
pub mod file;
pub mod file_async;
pub mod null;
pub mod raw;

pub struct Node {
    pub name: String,

    driver: Box<dyn NodeDriverData + Send + Sync>,

    pub limits: NodeLimits,
    queue_count: usize,
}

pub struct NodeLimits {
    pub size: AtomicU64,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub struct NodeConfig {
    node_name: String,

    #[serde(flatten)]
    driver: NodeDriverConfig,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "driver", rename_all = "kebab-case")]
pub enum NodeDriverConfig {
    Blkio(blkio::Config),
    File(file::Config),
    FileAsync(file_async::Config),
    Null(null::Config),
    Raw(raw::Config),
}

pub struct NodeBasicInfo {
    limits: NodeLimits,
    queue_count: usize,
}

#[async_trait]
pub trait NodeDriverData {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo>;

    fn read<'a>(
        &'a self,
        queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()>;

    fn write<'a>(&'a self, queue: usize, buf: &'a [u8], offset: u64) -> BlockFutureResult<'a, ()>;

    fn flush(&self, queue: usize) -> BlockFutureResult<'_, ()>;
}

impl Node {
    pub async fn new(
        opts: NodeConfig,
        node_map: &mut HashMap<String, Weak<Node>>,
    ) -> BlockResult<Arc<Node>> {
        if node_map.contains_key(&opts.node_name) {
            return Err(BlockError::from_desc(format!(
                "node-name \"{}\" is already in use",
                opts.node_name
            )));
        }

        let driver: Box<dyn NodeDriverData + Send + Sync> = match opts.driver {
            NodeDriverConfig::Blkio(o) => blkio::Data::new(o).await?,
            NodeDriverConfig::File(o) => file::Data::new(o).await?,
            NodeDriverConfig::FileAsync(o) => file_async::Data::new(o).await?,
            NodeDriverConfig::Null(o) => null::Data::new(o).await?,
            NodeDriverConfig::Raw(o) => raw::Data::new(o, node_map).await?,
        };

        let basic_info = driver.get_basic_info().await?;

        let node = Arc::new(Node {
            name: opts.node_name,
            driver,
            limits: basic_info.limits,
            queue_count: basic_info.queue_count,
        });

        let existing = node_map.insert(node.name.clone(), Arc::downgrade(&node));
        assert!(existing.is_none());

        Ok(node)
    }

    fn check_request(&self, buf: &[u8], offset: u64) -> BlockResult<()> {
        if offset + buf.len() as u64 > self.limits.size.load(Ordering::Relaxed) {
            return Err(BlockError::from_desc(format!(
                "Access [0x{:x}, 0x{:x}) is beyond the end of \"{}\" (size: 0x{:x})",
                offset,
                buf.len(),
                self.name,
                self.limits.size.load(Ordering::Relaxed)
            )));
        }

        Ok(())
    }

    pub fn read<'a>(
        &'a self,
        queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        if let Err(e) = self.check_request(buf, offset) {
            return Box::pin(async move { Err(e) });
        }

        self.driver.read(queue, buf, offset)
    }

    pub fn write<'a>(
        &'a self,
        queue: usize,
        buf: &'a [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        if let Err(e) = self.check_request(buf, offset) {
            return Box::pin(async move { Err(e) });
        }

        self.driver.write(queue, buf, offset)
    }

    pub fn flush(&self, queue: usize) -> BlockFutureResult<'_, ()> {
        self.driver.flush(queue)
    }

    pub fn queue_count(&self) -> usize {
        self.queue_count
    }
}
