#![feature(allocator_api)]
#![feature(drain_filter)]
#![feature(io_error_more)]
#![feature(linked_list_remove)]
#![allow(clippy::assertions_on_constants)]

mod error;
mod export;
mod helpers;
mod monitor;
mod node;

async fn run() -> error::BlockResult<()> {
    let mut mon = monitor::Monitor::new();

    mon.parse_args().await?;

    mon.wait()?;

    Ok(())
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    if let Err(e) = run().await {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}
