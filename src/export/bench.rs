use crate::helpers::BlockFutureResult;
use crate::node::Node;
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::pin::Pin;
use std::sync::atomic::Ordering;
use std::sync::Arc;
use std::task::{Context, Poll};

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case")]
pub enum IoRequestType {
    Read,
    Write,
    Flush,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    request_type: IoRequestType,
    simultaneous: Option<usize>,
    total: Option<usize>,
    request_size: Option<usize>,
    exit_when_done: Option<bool>,
}

pub struct RequestSlot {
    future: Option<BlockFutureResult<'static, ()>>,

    #[allow(dead_code)]
    backing_buffer: Vec<u8>,
    aligned_buffer: &'static mut [u8],
}

unsafe impl Send for RequestSlot {}

impl RequestSlot {
    fn with_buf(alignment: usize, length: usize) -> Self {
        let mut buffer = vec![42u8; alignment + length - 1];

        let mut head = alignment - (buffer.as_ptr() as usize) % alignment;
        if head == alignment {
            head = 0;
        }

        let aligned_buffer = unsafe {
            std::mem::transmute::<&'_ mut [u8], &'static mut [u8]>(
                &mut buffer[head..(head + length)],
            )
        };

        RequestSlot {
            future: None,
            backing_buffer: buffer,
            aligned_buffer,
        }
    }

    fn get_buffer(&mut self) -> &'static mut [u8] {
        unsafe { &mut *(self.aligned_buffer as *mut [u8]) }
    }
}

pub struct RandomIo {
    _node: Arc<Node>,
    node: &'static Node,

    reqs: Vec<RequestSlot>,
    simultaneous: usize,
    poll_index: usize,

    request_type: IoRequestType,
    request_size: usize,
    remaining_to_submit: usize,
    remaining_to_settle: usize,

    offset: u64,
    max_offset: u64,
}

impl Future for RandomIo {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let request_type = self.request_type;
        let node = self.node;

        let mut continuously_pending = 0;

        while continuously_pending < self.simultaneous && self.remaining_to_settle > 0 {
            let i = self.poll_index;
            self.poll_index = (self.poll_index + 1) % self.simultaneous;
            continuously_pending += 1;

            if let Some(fut) = self.reqs[i].future.as_mut() {
                match Future::poll(fut.as_mut(), cx) {
                    Poll::Pending => continue,
                    Poll::Ready(Err(e)) => eprintln!("I/O error: {}", e),
                    Poll::Ready(Ok(())) => (),
                }
                self.remaining_to_settle -= 1;
                self.reqs[i].future.take();
            }

            if self.remaining_to_submit == 0 {
                continue;
            }

            let ofs = self.offset;
            let buf = self.reqs[i].get_buffer();
            let fut_opt = &mut self.reqs[i].future;
            match request_type {
                IoRequestType::Read => fut_opt.replace(node.read(0, buf, ofs)),
                IoRequestType::Write => fut_opt.replace(node.write(0, buf, ofs)),
                IoRequestType::Flush => fut_opt.replace(node.flush(0)),
            };

            self.remaining_to_submit -= 1;
            self.offset += self.request_size as u64;
            if self.offset > self.max_offset {
                self.offset = 0;
            }

            continuously_pending = 0;
        }

        if self.remaining_to_settle == 0 {
            Poll::Ready(())
        } else {
            Poll::Pending
        }
    }
}

async fn run_bench(node: Arc<Node>, opts: Config) -> std::time::Duration {
    let total = opts.total.unwrap_or(1048576);
    let simultaneous = opts.simultaneous.unwrap_or(16);
    let request_type = opts.request_type;
    let request_size = opts.request_size.unwrap_or(4096);

    assert!(simultaneous <= total);

    let node_len = node.limits.size.load(Ordering::Relaxed);

    let n = unsafe { std::mem::transmute::<&'_ Node, &'static Node>(node.as_ref()) };

    let rio = RandomIo {
        _node: node,
        node: n,

        reqs: (0..simultaneous)
            .map(|_| RequestSlot::with_buf(request_size, request_size))
            .collect(),
        simultaneous,
        poll_index: 0,

        request_type,
        request_size,
        remaining_to_submit: total,
        remaining_to_settle: total,

        offset: 0,
        max_offset: node_len - request_size as u64,
    };

    let start = std::time::Instant::now();
    rio.await;
    let end = std::time::Instant::now();

    end - start
}

pub fn worker(node: Arc<Node>, opts: Config) {
    let total = opts.total.unwrap_or(1048576);
    let exit_when_done = opts.exit_when_done.unwrap_or(false);

    let duration = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(run_bench(node, opts));

    println!(
        "{} ops in {:?} = {} IOPS",
        total,
        duration,
        total as f64 / duration.as_secs_f64()
    );

    if exit_when_done {
        std::process::exit(0);
    }
}
