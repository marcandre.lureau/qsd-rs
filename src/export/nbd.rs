use crate::error::{BlockError, BlockResult};
use crate::node::Node;
use bincode::Options;
use serde::{Deserialize, Serialize};
use std::collections::LinkedList;
use std::io;
use std::mem::size_of;
use std::sync::atomic::{AtomicBool, AtomicU64, AtomicUsize, Ordering};
use std::sync::Arc;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{tcp, TcpListener, TcpStream};
use tokio::task::JoinHandle;

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {}

const COROUTINE_COUNT: usize = 16;

macro_rules! numerical_enum {
    (
        $(#[$attr:meta])*
        enum $enum_name:ident as $repr:tt {
            $($identifier:ident = $value:tt,)+
        }
    ) => {
        $(#[$attr])*
        #[repr($repr)]
        enum $enum_name {
            $($identifier = $value,)+
        }

        impl TryFrom<$repr> for $enum_name {
            type Error = BlockError;
            fn try_from(val: $repr) -> BlockResult<Self> {
                match val {
                    $($value => Ok($enum_name::$identifier),)*
                    _ => Err(BlockError::from_desc(format!(
                        "Invalid value for {}: {:x}",
                        stringify!($enum_name),
                        val
                    ))),
                }
            }
        }
    }
}

trait FlatSize {
    const SIZE: usize;
}

// TODO: Should be a procedural (derivable) macro
macro_rules! flat_size {
    (
        $(#[$attr:meta])*
        struct $struct_name:ident {
            $($identifier:ident: $type:ty,)+
        }
    ) => {
        $(#[$attr])*
        struct $struct_name {
            $($identifier: $type,)+
        }

        impl FlatSize for $struct_name {
            const SIZE: usize = $(size_of::<$type>()+)+ 0;
        }
    }
}

struct NbdJoinableConn {
    settled: Arc<AtomicBool>,
    handle: JoinHandle<()>,
}

struct NbdServer {
    node: Arc<Node>,
    listener: TcpListener,

    max_settled_cons: usize,

    conn_threads: LinkedList<NbdJoinableConn>,
    settled_conn_count: Arc<AtomicUsize>,
}

type NbdBincode = bincode::config::WithOtherEndian<
    bincode::config::WithOtherIntEncoding<
        bincode::config::DefaultOptions,
        bincode::config::FixintEncoding,
    >,
    bincode::config::BigEndian,
>;

struct NbdJoinableRequest {
    settled: Arc<AtomicBool>,
    handle: JoinHandle<()>,
}

struct NbdRequestTicket {
    #[allow(dead_code)]
    ticket: tokio::sync::OwnedSemaphorePermit,

    queue_i: usize,
    queues_used: Arc<AtomicU64>,

    settled: Arc<AtomicBool>,
    settled_req_count: Arc<AtomicUsize>,
}

struct NbdConnection {
    node: Arc<Node>,
    con_r: tcp::OwnedReadHalf,
    con_w: Arc<tokio::sync::Mutex<tcp::OwnedWriteHalf>>,
    bincode: NbdBincode,
    disconnect: Arc<AtomicBool>,

    max_settled_requests: usize,
    spawn_limit: Arc<tokio::sync::Semaphore>,

    queues_used: Arc<AtomicU64>,

    req_threads: LinkedList<NbdJoinableRequest>,
    settled_req_count: Arc<AtomicUsize>,
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdServerHandshake {
        magic1: u64,
        magic2: u64,
        flags: u16,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdClientFlags {
        flags: u32,
    }
}

numerical_enum! {
    #[derive(Clone, Copy)]
    enum NbdOpt as u32 {
        Go = 7,
        StructuredReply = 8,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdClientOpt {
        magic: u64,
        option: u32,
        data_length: u32,
    }
}

numerical_enum! {
    #[derive(Clone, Copy)]
    enum NbdReplyType as u32 {
        Ack = 1,
        Server = 2,
        Info = 3,
        MetaContext = 4,

        ErrUnsup = 0x80000001,
        ErrPolicy = 0x80000002,
        ErrInvalid = 0x80000003,
        ErrPlatform = 0x80000004,
        ErrTlsReqd = 0x80000005,
        ErrUnknown = 0x80000006,
        ErrShutdown = 0x80000007,
        ErrBlockSizeReqd = 0x80000008,
        ErrTooBig = 0x80000009,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdServerReply {
        magic: u64,
        client_option: u32,
        reply_type: u32,
        data_length: u32,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdInfoExport {
        info_type: u16,
        export_size: u64,
        transmission_flags: u16,
    }
}

numerical_enum! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    enum NbdRequestType as u16 {
        Read = 0,
        Write = 1,
        Disc = 2,
        Flush = 3,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdRequest {
        magic: u32,
        command_flags: u16,
        request_type: u16,
        handle: u64,
        offset: u64,
        length: u32,
    }
}

numerical_enum! {
    #[derive(Clone, Copy)]
    enum NbdResult as u32 {
        Ok = 0,
        Eperm = 1,
        Eio = 5,
        Enomem = 12,
        Einval = 22,
        Enospc = 28,
        Eoverflow = 75,
        Enotsup = 95,
        Eshutdown = 108,
    }
}

flat_size! {
    #[derive(Deserialize, Serialize)]
    struct NbdSimpleReply {
        magic: u32,
        error: u32,
        handle: u64,
    }
}

async fn nbd_main_loop(node: Arc<Node>) {
    let mut s = NbdServer::new(node, "127.0.0.1", 10809).await.unwrap();

    loop {
        if let Err(e) = s.handle_client().await {
            eprintln!("Client error: {}", e);
        }

        s.settle_cons().await;
    }
}

pub fn worker(node: Arc<Node>, _opts: Config) {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(nbd_main_loop(node));
}

impl NbdServer {
    async fn new(node: Arc<Node>, host: &str, port: u16) -> BlockResult<Self> {
        let listener = TcpListener::bind(format!("{}:{}", host, port)).await?;
        Ok(NbdServer {
            node,
            listener,
            max_settled_cons: 1,
            conn_threads: LinkedList::new(),
            settled_conn_count: Arc::new(AtomicUsize::new(0)),
        })
    }

    async fn handle_client(&mut self) -> BlockResult<()> {
        let (socket, _) = self.listener.accept().await?;

        let c = NbdConnection::from(socket, Arc::clone(&self.node));
        let settled = Arc::new(AtomicBool::new(false));
        let handle =
            tokio::spawn(c.handle(Arc::clone(&settled), Arc::clone(&self.settled_conn_count)));

        self.conn_threads
            .push_back(NbdJoinableConn { settled, handle });

        Ok(())
    }

    async fn settle_cons(&mut self) {
        while self.settled_conn_count.load(Ordering::Relaxed) > self.max_settled_cons {
            let joinable: LinkedList<NbdJoinableConn> = self
                .conn_threads
                .drain_filter(|h| h.settled.load(Ordering::Relaxed))
                .collect();

            let mut count = 0;
            for handle in joinable {
                handle.handle.await.unwrap();
                count += 1;
            }

            self.settled_conn_count.fetch_sub(count, Ordering::Relaxed);
        }
    }
}

impl NbdConnection {
    fn from(con: TcpStream, node: Arc<Node>) -> Self {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let (r, w) = con.into_split();

        // So queues_used can track all in a u64
        assert!(COROUTINE_COUNT <= 64);

        NbdConnection {
            node,
            con_r: r,
            con_w: Arc::new(tokio::sync::Mutex::new(w)),
            bincode,
            disconnect: Arc::new(AtomicBool::new(false)),

            max_settled_requests: 64,
            spawn_limit: Arc::new(tokio::sync::Semaphore::new(COROUTINE_COUNT)),

            queues_used: Arc::new(AtomicU64::new(0)),

            req_threads: LinkedList::new(),
            settled_req_count: Arc::new(AtomicUsize::new(0)),
        }
    }

    async fn handle(mut self, settled: Arc<AtomicBool>, settled_conn_count: Arc<AtomicUsize>) {
        if let Err(e) = self.do_handle().await {
            eprintln!("Client error: {}", e);
        }

        settled_conn_count.fetch_add(1, Ordering::Relaxed);
        settled.store(true, Ordering::Relaxed);
    }

    async fn do_handle(&mut self) -> BlockResult<()> {
        self.handshake_phase().await?;
        self.transmission_phase().await?;

        Ok(())
    }

    async fn handshake_phase(&mut self) -> BlockResult<()> {
        let handshake_buf = self.bincode.serialize(&NbdServerHandshake::new(1))?;
        self.con_w.lock().await.write_all(&handshake_buf).await?;

        let client_flags: NbdClientFlags = {
            let mut client_flags_buf = vec![0u8; NbdClientFlags::SIZE];
            self.con_r.read_exact(&mut client_flags_buf).await?;
            self.bincode.deserialize(&client_flags_buf)?
        };

        client_flags.check()?;

        while self.handle_client_opt().await? {}

        Ok(())
    }

    async fn handle_client_opt(&mut self) -> BlockResult<bool> {
        let (client_opt, _client_opt_data) = {
            let mut client_opt_buf = vec![0u8; NbdClientOpt::SIZE];
            self.con_r.read_exact(&mut client_opt_buf).await?;
            let client_opt: NbdClientOpt = self.bincode.deserialize(&client_opt_buf)?;

            client_opt.check()?;

            let mut client_opt_data = vec![0u8; client_opt.data_length.try_into()?];
            if client_opt.data_length > 0 {
                self.con_r.read_exact(&mut client_opt_data).await?;
            }

            (client_opt, client_opt_data)
        };

        let continue_loop = match client_opt.option.try_into() {
            Ok(NbdOpt::Go) => {
                let nbd_info_export =
                    NbdInfoExport::new(self.node.limits.size.load(Ordering::Relaxed), 1);
                let nbd_info_export_buf = self.bincode.serialize(&nbd_info_export)?;
                self.reply_data(&client_opt, NbdReplyType::Info, Some(&nbd_info_export_buf))
                    .await?;
                self.reply(&client_opt, NbdReplyType::Ack).await?;
                false
            }

            Ok(NbdOpt::StructuredReply) => {
                self.reply(&client_opt, NbdReplyType::ErrUnsup).await?;
                true
            }

            Err(_) => {
                println!("DEBUG: Unknown NBD client option {}", client_opt.option);
                self.reply(&client_opt, NbdReplyType::ErrUnsup).await?;
                true
            }
        };

        Ok(continue_loop)
    }

    async fn reply_data(
        &self,
        client_opt: &NbdClientOpt,
        reply_type: NbdReplyType,
        data: Option<&Vec<u8>>,
    ) -> BlockResult<()> {
        let mut con_w = self.con_w.lock().await;

        let data_length = match &data {
            Some(data) => data.len().try_into()?,
            None => 0,
        };
        let reply = NbdServerReply::new(client_opt.option, reply_type, data_length);
        let reply_buf = self.bincode.serialize(&reply)?;
        con_w.write_all(&reply_buf).await?;

        if let Some(data) = data {
            con_w.write_all(data).await?;
        }

        Ok(())
    }

    async fn reply(&self, client_opt: &NbdClientOpt, reply_type: NbdReplyType) -> BlockResult<()> {
        self.reply_data(client_opt, reply_type, None).await
    }

    async fn transmission_phase(&mut self) -> BlockResult<()> {
        while !self.disconnect.load(Ordering::Relaxed) {
            let (request, data) = match self.transmission_get_request().await {
                Ok((r, d)) => (r, d),
                Err(e) => {
                    self.transmission_settle_all().await;
                    if self.disconnect.load(Ordering::Relaxed) {
                        /* Ignore error when already disconnecting */
                        return Ok(());
                    } else {
                        return Err(e);
                    }
                }
            };

            let ticket = Arc::clone(&self.spawn_limit).acquire_owned().await.unwrap();

            let queue_i = self.queues_used.load(Ordering::Relaxed).trailing_ones() as usize;
            self.queues_used.fetch_or(1 << queue_i, Ordering::Relaxed);

            let settled = Arc::new(AtomicBool::new(false));

            let ticket = NbdRequestTicket {
                ticket,

                queue_i,
                queues_used: Arc::clone(&self.queues_used),

                settled: Arc::clone(&settled),
                settled_req_count: Arc::clone(&self.settled_req_count),
            };

            let handle = tokio::spawn(Self::transmission_process(
                Arc::clone(&self.con_w),
                Arc::clone(&self.node),
                request,
                data,
                ticket,
                Arc::clone(&self.disconnect),
            ));

            self.req_threads
                .push_back(NbdJoinableRequest { settled, handle });

            self.transmission_settle_ready().await;
        }

        self.transmission_settle_all().await;

        Ok(())
    }

    async fn transmission_settle_all(&mut self) {
        while let Some(handle) = self.req_threads.pop_front() {
            handle.handle.await.unwrap();
        }
    }

    async fn transmission_settle_ready(&mut self) {
        while self.settled_req_count.load(Ordering::Relaxed) > self.max_settled_requests {
            let joinable: LinkedList<NbdJoinableRequest> = self
                .req_threads
                .drain_filter(|h| h.settled.load(Ordering::Relaxed))
                .collect();

            let mut count = 0;
            for handle in joinable {
                handle.handle.await.unwrap();
                count += 1;
            }

            self.settled_req_count.fetch_sub(count, Ordering::Relaxed);
        }
    }

    async fn transmission_get_request(&mut self) -> BlockResult<(NbdRequest, Option<Vec<u8>>)> {
        let mut request_buf = vec![0u8; NbdRequest::SIZE];
        self.con_r.read_exact(&mut request_buf).await?;
        let request: NbdRequest = self.bincode.deserialize(&request_buf)?;

        request.check()?;

        let data = if request.request_type() == NbdRequestType::Write {
            let mut data = vec![0u8; request.length.try_into()?];
            self.con_r.read_exact(&mut data).await?;
            Some(data)
        } else {
            None
        };

        Ok((request, data))
    }

    #[allow(clippy::too_many_arguments)]
    async fn transmission_process(
        con_w: Arc<tokio::sync::Mutex<tcp::OwnedWriteHalf>>,
        node: Arc<Node>,
        request: NbdRequest,
        data: Option<Vec<u8>>,
        ticket: NbdRequestTicket,
        disconnect: Arc<AtomicBool>,
    ) {
        let result = Self::transmission_do_process(
            con_w,
            node,
            request,
            data,
            ticket,
            Arc::clone(&disconnect),
        )
        .await;

        if let Err(e) = result {
            eprintln!("Transmission error: {}", e);
            disconnect.store(true, Ordering::Relaxed);
        }
    }

    async fn transmission_do_process(
        con_w: Arc<tokio::sync::Mutex<tcp::OwnedWriteHalf>>,
        node: Arc<Node>,
        request: NbdRequest,
        data: Option<Vec<u8>>,
        ticket: NbdRequestTicket,
        disconnect: Arc<AtomicBool>,
    ) -> BlockResult<()> {
        let bincode = bincode::DefaultOptions::new()
            .with_fixint_encoding()
            .with_big_endian();

        let queue = ticket.queue_i % node.queue_count();

        match request.request_type() {
            NbdRequestType::Read => {
                let mut data = vec![0u8; request.length.try_into()?];
                match node.read(queue, &mut data, request.offset).await {
                    Ok(()) => {
                        let reply = NbdSimpleReply::new(request, NbdResult::Ok);
                        let reply_buf = bincode.serialize(&reply)?;

                        let mut con_w = con_w.lock().await;
                        con_w.write_all(&reply_buf).await?;
                        con_w.write_all(&data).await?;
                    }

                    Err(e) => {
                        let reply = NbdSimpleReply::new(request, e.into());
                        let reply_buf = bincode.serialize(&reply)?;
                        con_w.lock().await.write_all(&reply_buf).await?;
                    }
                }
            }

            NbdRequestType::Write => {
                let data = data.unwrap();
                let err: NbdResult = node.write(queue, &data, request.offset).await.into();
                let reply = NbdSimpleReply::new(request, err);
                let reply_buf = bincode.serialize(&reply)?;
                con_w.lock().await.write_all(&reply_buf).await?;
            }

            NbdRequestType::Disc => disconnect.store(true, Ordering::Relaxed),

            NbdRequestType::Flush => {
                let err: NbdResult = node.flush(queue).await.into();
                let reply = NbdSimpleReply::new(request, err);
                let reply_buf = bincode.serialize(&reply)?;
                con_w.lock().await.write_all(&reply_buf).await?;
            }
        }

        con_w.lock().await.flush().await?;

        Ok(())
    }
}

impl NbdServerHandshake {
    fn new(flags: u16) -> Self {
        NbdServerHandshake {
            magic1: 0x4e42444d41474943,
            magic2: 0x49484156454f5054,
            flags,
        }
    }
}

impl NbdClientFlags {
    fn check(&self) -> BlockResult<()> {
        if self.flags == 1 {
            Ok(())
        } else {
            Err(BlockError::from_desc(format!(
                "Unrecognized client flags: {:x}",
                self.flags
            )))
        }
    }
}

impl NbdClientOpt {
    fn check(&self) -> BlockResult<()> {
        if self.magic != 0x49484156454f5054 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized client option magic: {:x}",
                self.magic
            )));
        }

        if self.data_length > 1024 {
            return Err(BlockError::from_desc(format!(
                "Client option data too long: {}",
                self.data_length
            )));
        }

        Ok(())
    }
}

impl NbdServerReply {
    fn new(client_option: u32, reply_type: NbdReplyType, data_length: u32) -> Self {
        NbdServerReply {
            magic: 0x3e889045565a9,
            client_option,
            reply_type: reply_type as u32,
            data_length,
        }
    }
}

impl NbdInfoExport {
    fn new(export_size: u64, transmission_flags: u16) -> Self {
        NbdInfoExport {
            info_type: 0,
            export_size,
            transmission_flags,
        }
    }
}

impl NbdRequest {
    fn check(&self) -> BlockResult<()> {
        if self.magic != 0x25609513 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized request magic: {:x}",
                self.magic
            )));
        }

        if self.command_flags != 0 {
            return Err(BlockError::from_desc(format!(
                "Unrecognized command flags: {:x}",
                self.command_flags
            )));
        }

        let request: BlockResult<NbdRequestType> = self.request_type.try_into();
        if request.is_err() {
            return Err(BlockError::from_desc(format!(
                "Unrecognized request: {:x}",
                self.request_type
            )));
        }

        if self.length > 32 * 1024 * 1024 {
            return Err(BlockError::from_desc(format!(
                "Request is too long: {}",
                self.length
            )));
        }

        Ok(())
    }

    fn request_type(&self) -> NbdRequestType {
        self.request_type.try_into().unwrap()
    }
}

impl From<BlockError> for NbdResult {
    fn from(err: BlockError) -> Self {
        match err.into_inner().kind() {
            io::ErrorKind::PermissionDenied => NbdResult::Eperm,
            io::ErrorKind::InvalidData | io::ErrorKind::InvalidInput => NbdResult::Einval,
            io::ErrorKind::StorageFull => NbdResult::Enospc,
            io::ErrorKind::OutOfMemory => NbdResult::Enomem,
            _ => NbdResult::Eio,
        }
    }
}

impl<T> From<BlockResult<T>> for NbdResult {
    fn from(res: BlockResult<T>) -> Self {
        match res {
            Ok(_) => NbdResult::Ok,
            Err(e) => e.into(),
        }
    }
}

impl NbdSimpleReply {
    fn new(request: NbdRequest, error: NbdResult) -> Self {
        NbdSimpleReply {
            magic: 0x67446698,
            error: error as u32,
            handle: request.handle,
        }
    }
}

impl Drop for NbdRequestTicket {
    fn drop(&mut self) {
        self.queues_used
            .fetch_and(!(1 << self.queue_i), Ordering::Relaxed);

        self.settled_req_count.fetch_add(1, Ordering::Relaxed);
        self.settled.store(true, Ordering::Relaxed);
    }
}
