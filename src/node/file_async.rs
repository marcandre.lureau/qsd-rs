use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use crate::node::{NodeBasicInfo, NodeDriverData, NodeLimits};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::future::Future;
use std::io;
use std::marker::PhantomData;
use std::os::unix::io::{AsRawFd, RawFd};
use std::pin::Pin;
use std::sync::atomic::AtomicU64;
use std::sync::mpsc::{self, Sender};
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll, Waker};
use std::thread::{self, JoinHandle};
use tokio::fs::{self, OpenOptions};

extern "C" {
    fn pread(
        fildes: libc::c_int,
        buf: *mut libc::c_void,
        nbyte: libc::size_t,
        offset: libc::off_t,
    ) -> libc::ssize_t;

    fn pwrite(
        fildes: libc::c_int,
        buf: *const libc::c_void,
        nbyte: libc::size_t,
        offset: libc::off_t,
    ) -> libc::ssize_t;

    fn fsync(fildes: libc::c_int) -> libc::c_int;
}

pub struct Data {
    file: fs::File,

    pread_pool: IoThreadPool<Pread>,
    pwrite_pool: IoThreadPool<Pwrite>,
    flush_pool: IoThreadPool<Flush>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
    pub direct: Option<bool>,
}

impl Data {
    pub async fn new(opts: Config) -> BlockResult<Box<Self>> {
        let mut file_opts = OpenOptions::new();
        file_opts.read(true).write(true);
        if opts.direct.unwrap_or(false) {
            file_opts.custom_flags(libc::O_DIRECT);
        }
        let file = file_opts.open(opts.filename).await?;

        Ok(Box::new(Data {
            file,
            pread_pool: IoThreadPool::new(16),
            pwrite_pool: IoThreadPool::new(16),
            flush_pool: IoThreadPool::new(4),
        }))
    }
}

#[derive(Default)]
struct DoneSignal {
    done: bool,
    result: Option<BlockResult<()>>,
    waker: Option<Waker>,
}

struct Pread {
    fd: RawFd,
    offset: u64,
}

struct Pwrite {
    fd: RawFd,
    offset: u64,
}

struct Flush {
    fd: RawFd,
}

enum BlockingIoBuffer<'a> {
    Mutable(&'a mut [u8]),
    Immutable(&'a [u8]),
}

trait BlockingIo {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()>;
}

#[derive(Default)]
struct IoWrapper<'a> {
    ds: Arc<Mutex<DoneSignal>>,

    // Marker for the buffer used internally
    _lifetime: PhantomData<&'a mut [u8]>,
}

impl Pread {
    fn new(fd: RawFd, offset: u64) -> Self {
        Pread { fd, offset }
    }
}

impl BlockingIo for Pread {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()> {
        let buf = match buf {
            Some(BlockingIoBuffer::Mutable(buf)) => buf,
            _ => {
                return Err(BlockError::from_desc(
                    "Invalid or no buffer given to pread".into(),
                ))
            }
        };

        let mut completed = 0usize;
        let len = buf.len();

        while completed < len {
            let slice = &mut buf[completed..len];
            let off = self.offset + completed as u64;

            let bytes_read = unsafe {
                pread(
                    self.fd,
                    slice.as_mut_ptr() as *mut libc::c_void,
                    slice.len(),
                    off.try_into()?,
                )
            };
            if bytes_read < 0 {
                return Err(io::Error::last_os_error().into());
            }

            completed += bytes_read as usize;
        }

        Ok(())
    }
}

impl Pwrite {
    fn new(fd: RawFd, offset: u64) -> Self {
        Pwrite { fd, offset }
    }
}

impl BlockingIo for Pwrite {
    fn process(&mut self, buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()> {
        let buf = match buf {
            Some(BlockingIoBuffer::Immutable(buf)) => buf,
            _ => {
                return Err(BlockError::from_desc(
                    "Invalid or no buffer given to pwrite".into(),
                ))
            }
        };

        let mut completed = 0usize;
        let len = buf.len();

        while completed < len {
            let slice = &buf[completed..len];
            let off = self.offset + completed as u64;

            let bytes_written = unsafe {
                pwrite(
                    self.fd,
                    slice.as_ptr() as *const libc::c_void,
                    slice.len(),
                    off.try_into()?,
                )
            };
            if bytes_written < 0 {
                return Err(io::Error::last_os_error().into());
            }

            completed += bytes_written as usize;
        }

        Ok(())
    }
}

impl Flush {
    fn new(fd: RawFd) -> Self {
        Flush { fd }
    }
}

impl BlockingIo for Flush {
    fn process(&mut self, _buf: Option<BlockingIoBuffer<'_>>) -> BlockResult<()> {
        let ret = unsafe { fsync(self.fd) };
        if ret < 0 {
            Err(io::Error::last_os_error().into())
        } else {
            Ok(())
        }
    }
}

struct IoWorkUnit<T: 'static + BlockingIo + Send + Sync> {
    obj: T,
    buf: Option<BlockingIoBuffer<'static>>,
    ds: Arc<Mutex<DoneSignal>>,
}

struct IoThreadPool<T: 'static + BlockingIo + Send + Sync> {
    #[allow(dead_code)]
    handles: Vec<JoinHandle<()>>,
    input: Mutex<Sender<IoWorkUnit<T>>>,
}

impl<T: 'static + BlockingIo + Send + Sync> IoThreadPool<T> {
    fn new(count: usize) -> Self {
        let (s, r) = mpsc::channel::<IoWorkUnit<T>>();
        let r = Arc::new(Mutex::new(r));

        let handles = (0..count)
            .map(|_| {
                let r = Arc::clone(&r);
                thread::spawn(move || loop {
                    let mut unit = r.lock().unwrap().recv().unwrap();
                    let result = unit.obj.process(unit.buf);
                    let waker = {
                        let mut ds = unit.ds.lock().unwrap();
                        ds.result = Some(result);
                        ds.done = true;
                        ds.waker.clone()
                    };
                    if let Some(waker) = waker {
                        waker.wake_by_ref();
                    }
                })
            })
            .collect();

        IoThreadPool {
            handles,
            input: Mutex::new(s),
        }
    }

    fn do_submit<'a>(&self, obj: T, buf: Option<BlockingIoBuffer<'a>>) -> IoWrapper<'a> {
        let buf = buf.map(|buf| unsafe {
            std::mem::transmute::<BlockingIoBuffer<'a>, BlockingIoBuffer<'static>>(buf)
        });
        let unit = IoWorkUnit {
            obj,
            buf,
            ds: Default::default(),
        };

        let wrapper = IoWrapper {
            ds: Arc::clone(&unit.ds),
            _lifetime: PhantomData,
        };

        let s = { self.input.lock().unwrap().clone() };
        s.send(unit).unwrap();

        wrapper
    }

    fn submit(&self, obj: T) -> IoWrapper<'static> {
        self.do_submit(obj, None)
    }

    fn submit_buf<'a>(&self, obj: T, buf: &'a [u8]) -> IoWrapper<'a> {
        self.do_submit(obj, Some(BlockingIoBuffer::Immutable(buf)))
    }

    fn submit_mut_buf<'a>(&self, obj: T, buf: &'a mut [u8]) -> IoWrapper<'a> {
        self.do_submit(obj, Some(BlockingIoBuffer::Mutable(buf)))
    }
}

impl<'a> Future for IoWrapper<'a> {
    type Output = BlockResult<()>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let result = {
            let mut ds = self.ds.lock().unwrap();
            if ds.result.is_none() && ds.waker.is_none() {
                ds.waker = Some(cx.waker().clone());
            }
            ds.result.take()
        };

        if let Some(result) = result {
            Poll::Ready(result)
        } else {
            Poll::Pending
        }
    }
}

impl<'a> Drop for IoWrapper<'a> {
    fn drop(&mut self) {
        // Assert that the thread does not outlive the lifetime of this object
        assert!(self.ds.lock().unwrap().done);
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(self.file.metadata().await?.len()),
            },
            queue_count: 1,
        })
    }

    fn read<'a>(
        &'a self,
        _queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        let p = Pread::new(self.file.as_raw_fd(), offset);
        Box::pin(self.pread_pool.submit_mut_buf(p, buf))
    }

    fn write<'a>(&'a self, _queue: usize, buf: &'a [u8], offset: u64) -> BlockFutureResult<'a, ()> {
        let p = Pwrite::new(self.file.as_raw_fd(), offset);
        Box::pin(self.pwrite_pool.submit_buf(p, buf))
    }

    fn flush(&self, _queue: usize) -> BlockFutureResult<'_, ()> {
        let f = Flush::new(self.file.as_raw_fd());
        Box::pin(self.flush_pool.submit(f))
    }
}
