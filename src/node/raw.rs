use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use crate::node::{Node, NodeBasicInfo, NodeDriverData, NodeLimits};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Weak};

pub struct Data {
    file: Arc<Node>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub file: String,
}

impl Data {
    pub async fn new(
        opts: Config,
        node_map: &mut HashMap<String, Weak<Node>>,
    ) -> BlockResult<Box<Self>> {
        let file = match node_map.get(&opts.file).and_then(|n| n.upgrade()) {
            Some(n) => n,
            None => {
                return Err(BlockError::from_desc(format!(
                    "Node \"{}\" not found",
                    opts.file
                )))
            }
        };

        Ok(Box::new(Data { file }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(self.file.limits.size.load(Ordering::Relaxed)),
            },
            queue_count: self.file.queue_count(),
        })
    }

    fn read<'a>(
        &'a self,
        queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        self.file.read(queue, buf, offset)
    }

    fn write<'a>(&'a self, queue: usize, buf: &'a [u8], offset: u64) -> BlockFutureResult<'a, ()> {
        self.file.write(queue, buf, offset)
    }

    fn flush(&self, queue: usize) -> BlockFutureResult<'_, ()> {
        self.file.flush(queue)
    }
}
