use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use crate::node::{NodeBasicInfo, NodeDriverData, NodeLimits};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::fs::{self, OpenOptions};
use std::io::SeekFrom;
use std::io::{Read, Seek, Write};
use std::os::unix::fs::OpenOptionsExt;
use std::sync::atomic::AtomicU64;
use std::sync::Mutex;

pub struct Data {
    files: Vec<Mutex<fs::File>>,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
    pub queue_count: Option<usize>,
    pub direct: Option<bool>,
}

impl Data {
    pub async fn new(opts: Config) -> BlockResult<Box<Self>> {
        let queue_count = opts.queue_count.unwrap_or(1);
        if queue_count < 1 {
            return Err(BlockError::from_desc("queue-count must not be 0".into()));
        }

        let mut files = Vec::new();
        for _ in 0..queue_count {
            let mut file_opts = OpenOptions::new();
            file_opts.read(true).write(true);
            if opts.direct.unwrap_or(false) {
                file_opts.custom_flags(libc::O_DIRECT);
            }
            let f = file_opts.open(&opts.filename)?;
            files.push(Mutex::new(f));
        }

        Ok(Box::new(Data { files }))
    }

    async fn do_read(&self, queue: usize, buf: &mut [u8], offset: u64) -> BlockResult<()> {
        let mut file = self.files[queue].lock().unwrap();

        file.seek(SeekFrom::Start(offset))?;
        file.read_exact(buf)?;

        Ok(())
    }

    async fn do_write(&self, queue: usize, buf: &[u8], offset: u64) -> BlockResult<()> {
        let mut file = self.files[queue].lock().unwrap();

        file.seek(SeekFrom::Start(offset))?;
        file.write_all(buf)?;

        Ok(())
    }

    async fn do_flush(&self, queue: usize) -> BlockResult<()> {
        let file = self.files[queue].lock().unwrap();

        file.sync_data()?;

        Ok(())
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(self.files[0].lock().unwrap().metadata()?.len()),
            },
            queue_count: 1,
        })
    }

    fn read<'a>(
        &'a self,
        queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(self.do_read(queue, buf, offset))
    }

    fn write<'a>(&'a self, queue: usize, buf: &'a [u8], offset: u64) -> BlockFutureResult<'a, ()> {
        Box::pin(self.do_write(queue, buf, offset))
    }

    fn flush(&self, _queue: usize) -> BlockFutureResult<'_, ()> {
        Box::pin(async move {
            let results =
                futures::future::join_all((0..self.files.len()).map(|i| self.do_flush(i))).await;
            for result in results {
                result?;
            }
            Ok(())
        })
    }
}
