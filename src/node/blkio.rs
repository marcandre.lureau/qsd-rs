use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use crate::node::{NodeBasicInfo, NodeDriverData, NodeLimits};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::atomic::AtomicU64;
use std::sync::{Arc, Mutex};

pub struct Data {
    blkio: Arc<Mutex<blkio::Blkio>>,
    queues: Vec<blkio_async::AsyncBlkioq>,
}

unsafe impl Send for Data {}
unsafe impl Sync for Data {}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub filename: String,
    pub queue_count: Option<usize>,
    pub direct: Option<bool>,
}

impl Data {
    pub async fn new(opts: Config) -> BlockResult<Box<Self>> {
        let queue_count = opts.queue_count.unwrap_or(1);
        if queue_count < 1 {
            return Err(BlockError::from_desc("queue-count must not be 0".into()));
        }

        let mut blkio = blkio::Blkio::new("io_uring")?;
        blkio.set_str("path", &opts.filename)?;
        blkio.set_bool("direct", opts.direct.unwrap_or(false))?;
        blkio.connect()?;
        assert!(!(blkio.get_bool("needs-mem-regions")?));
        blkio.set_i32("num-queues", queue_count.try_into()?)?;
        blkio.start()?;

        let blkio = Arc::new(Mutex::new(blkio));

        let queues = (0..queue_count)
            .map(|i| blkio_async::AsyncBlkioq::new(Arc::clone(&blkio), i))
            .collect();

        Ok(Box::new(Data { blkio, queues }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        let blkio = self.blkio.lock().unwrap();
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(blkio.get_u64("capacity")?),
            },
            queue_count: 1,
        })
    }

    fn read<'a>(
        &'a self,
        queue: usize,
        buf: &'a mut [u8],
        offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        let fut = self.queues[queue].read(offset, buf, blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }

    fn write<'a>(&'a self, queue: usize, buf: &'a [u8], offset: u64) -> BlockFutureResult<'a, ()> {
        let fut = self.queues[queue].write(offset, buf, blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }

    fn flush(&self, queue: usize) -> BlockFutureResult<'_, ()> {
        let fut = self.queues[queue].flush(blkio::ReqFlags::empty());

        Box::pin(async move {
            fut.await?;
            Ok(())
        })
    }
}
