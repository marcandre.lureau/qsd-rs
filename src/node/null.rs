use crate::error::{BlockError, BlockResult};
use crate::helpers::BlockFutureResult;
use crate::node::{NodeBasicInfo, NodeDriverData, NodeLimits};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::atomic::AtomicU64;

pub struct Data {
    opts: Config,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Config {
    pub size: u64,
    pub queue_count: Option<usize>,
}

impl Data {
    pub async fn new(opts: Config) -> BlockResult<Box<Self>> {
        if matches!(opts.queue_count, Some(1)) {
            return Err(BlockError::from_desc("queue-count must not be 0".into()));
        }

        Ok(Box::new(Data { opts }))
    }
}

#[async_trait]
impl NodeDriverData for Data {
    async fn get_basic_info(&self) -> BlockResult<NodeBasicInfo> {
        Ok(NodeBasicInfo {
            limits: NodeLimits {
                size: AtomicU64::new(self.opts.size),
            },
            queue_count: self.opts.queue_count.unwrap_or(1),
        })
    }

    fn read<'a>(
        &'a self,
        _queue: usize,
        buf: &'a mut [u8],
        _offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(async {
            buf.fill(0);
            Ok(())
        })
    }

    fn write<'a>(
        &'a self,
        _queue: usize,
        _buf: &'a [u8],
        _offset: u64,
    ) -> BlockFutureResult<'a, ()> {
        Box::pin(async { Ok(()) })
    }

    fn flush(&self, _queue: usize) -> BlockFutureResult<'_, ()> {
        Box::pin(async { Ok(()) })
    }
}
