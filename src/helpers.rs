use crate::error::BlockResult;
use std::future::Future;
use std::pin::Pin;

/// Describes `Future` objects that will return a `BlockResult<T>`.  This should be the return type
/// of all quasi-async I/O functions returning a `BlockResult<_>`, i.e. functions that are not
/// async, but can be used as such.  For example, the following two functions are semantically
/// similar:
///
/// ```rust
/// async fn foo(buf: &[u8]) -> BlockResult<()>;
/// fn foo<'a>(buf: &'a [u8]) -> BlockFutureResult<'a, ()>;
/// ```
///
/// Except that the second function is guaranteed to return a boxed future, which is why we use
/// this style.
pub type BlockFutureResult<'a, T> = Pin<Box<dyn Future<Output = BlockResult<T>> + Send + 'a>>;
